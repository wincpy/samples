package com.widgets.big.android_gradle_eclipse;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends Activity {

	private static final String ARG_BARCODE = "barcode";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Button btnScanBarcode = (Button) findViewById(R.id.btnScanBarcode);
		btnScanBarcode.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startScan();
				
			}
		});
	}
	
	protected void startScan() {
		IntentIntegrator.initiateScan(this);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		IntentResult scanResult = IntentIntegrator.parseActivityResult(
				requestCode, resultCode, intent);
		if (scanResult != null) {
			Log.i("android-gradle-eclipse - MainActivity", scanResult.getContents());
			Toast.makeText(getApplicationContext(), "Scanned Barcode Number is : "+scanResult.getContents(), Toast.LENGTH_LONG).show();
		}
	}


}
